import axios from 'axios';
const http = 'https://www.googleapis.com/youtube/v3/';
const key = 'AIzaSyBivanNAVTudBAbtZnjiQk1mVMbyh2edUA';

/**
 * Получает ID-шники видео по введенной строке
 * @param {string} searchBy 
 * @returns {string[]}
 */
const getSnippet = async (searchBy) => {
  const result = await axios.get(http + 'search', {
    params: {
      part: 'snippet',
      key: key,
      q: searchBy,
      order: 'date',
      type: 'video',
      maxResults: 10
    }
  });
  if (!result) {
    return false;
  }
  console.log(result);

  return result.data.items;
}

/**
 * Делает запрос к ютуб по введенной строке
 * @param {string[]} ids
 */
const getStatistics= async (ids) => {
  const result = await axios.get(http + 'videos', {
    params: {
      part: 'statistics',
      id: ids,
      key: key,
      maxResults: 10
    }
  });

  return result.data.items;
}



export default {
  getSnippet,
  getStatistics
} 