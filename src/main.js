import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/tailwind.css'
import "vue-material-design-icons/styles.css";


import Magnify from 'vue-material-design-icons/Magnify.vue';
Vue.component('magnify', Magnify);

import Close from 'vue-material-design-icons/Close.vue';
Vue.component('close', Close);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
